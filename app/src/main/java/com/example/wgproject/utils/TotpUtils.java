package com.example.wgproject.utils;

import android.os.CountDownTimer;
import android.util.Log;

import com.example.wgproject.events.TotpEvent;

import org.greenrobot.eventbus.EventBus;

public class TotpUtils {
    private static TotpUtils mInstance;

    private CountDownTimer mCountDownGetOtp;

    private native byte[] generateOtp(String key);

    static {
        System.loadLibrary(Constants.JNI_LIB_NAME);
    }

    public static TotpUtils getInstance() {
        if (mInstance == null)
            mInstance = new TotpUtils();

        return mInstance;
    }

    private TotpUtils() {
    }

    public void recursivelyGenerateOtp(final String key) {
        if (mCountDownGetOtp != null)
            mCountDownGetOtp.cancel();

        mCountDownGetOtp = new CountDownTimer(Constants.UPDATE_INTERVAL, Constants.UPDATE_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                recursivelyGenerateOtp(key);
            }
        };
        mCountDownGetOtp.start();

        byte[] otpByteArray = null;

        if (key != null && !key.isEmpty())
            otpByteArray = generateOtp(key);

        if (otpByteArray != null) {
            String totp = generateTotp(otpByteArray);

            if (totp != null && !totp.isEmpty()) {
                EventBus.getDefault().post(new TotpEvent(totp));
                mCountDownGetOtp.start();
            }
        }
    }

    public void cancelTimer() {
        mCountDownGetOtp.cancel();
    }

    private String generateTotp(byte[] byteArray) {
        String byteArrayHexValue = TextUtils.bytesToHex(byteArray);
        String dbc1String = generateDBC1(byteArrayHexValue);
        String dbc2String = generateDBC2(dbc1String);

        long dbc2 = Long.parseLong(dbc2String, 16);

        Log.d(Constants.LOG_TAG, "DBC1: " + dbc1String);
        Log.d(Constants.LOG_TAG, "DBC2: " + dbc2String);

        String totp = parseTotp(dbc2);
        resultValidation(byteArray, totp);

        return totp;
    }

    private String generateDBC1(String byteArrayHexValue) {
        if (byteArrayHexValue != null && !byteArrayHexValue.isEmpty()) {
            String lastByte = byteArrayHexValue.substring(byteArrayHexValue.length() - 1);

            Log.d(Constants.LOG_TAG, TextUtils.separateHexBytes(byteArrayHexValue));

            int mineOffset = Integer.parseInt(lastByte, 16);
            int offsetInBytes = mineOffset * 2;

            return byteArrayHexValue.substring(offsetInBytes, offsetInBytes + 8);
        }

        return "";
    }

    private String generateDBC2(String dbc1String) {
        if (dbc1String != null && !dbc1String.isEmpty()) {
            String dbc1StringFirstByte = dbc1String.substring(0, 1);

            long dbc1FirstByte = Long.parseLong(dbc1StringFirstByte, 16);
            long clearedBit = dbc1FirstByte & ~(1 << 3);

            return clearedBit + dbc1String.substring(1);
        }

        return "";
    }

    private String parseTotp(long dbc2Decimal) {
        String decimalValue = String.valueOf(dbc2Decimal);
        String totp = decimalValue.substring(decimalValue.length() - 6);

        Log.d(Constants.LOG_TAG, "Generated TOTP: " + totp);

        return totp;
    }

    private void resultValidation(byte[] byteArray, String totp) {
        /*
         *
         * This code was found on the website: https://www.devmedia.com.br/algoritmo-de-geracao-de-chaves-de-validacao-baseadas-no-tempo-totp/32720
         * and it's algorithm is being used only for validation purposes
         *
         * */

        int offset = byteArray[byteArray.length - 1] & 0xf;
        int otp = ((byteArray[offset] & 0x7f) << 24) |
                ((byteArray[offset + 1] & 0xff) << 16) |
                ((byteArray[offset + 2] & 0xff) << 8) |
                (byteArray[offset + 3] & 0xff);

        otp = otp % 1000000;

        StringBuilder result = new StringBuilder(Long.toString(otp));
        while (result.length() < 6)
            result.insert(0, "0");

        Log.d(Constants.LOG_TAG, "Valid TOTP: " + result);
        Log.d(Constants.LOG_TAG, totp.contentEquals(result) ? "Result is OK!" : "Result is INVALID!");
        Log.d(Constants.LOG_TAG, "-------------------------------------------");
    }
}