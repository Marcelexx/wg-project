package com.example.wgproject.utils;

public class Constants {
    public static final String JNI_LIB_NAME = "otpjni-lib";
    public static final String LOG_TAG = "WatchGuard";
    public static final int UPDATE_INTERVAL = 30000; //30 seconds
    public static final int TICK_INTERVAL = 1000; //1 second
    public static final int MAX_PROGRESS = 100; //100 per cent
}