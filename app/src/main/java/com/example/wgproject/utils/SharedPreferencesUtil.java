package com.example.wgproject.utils;

import android.content.Context;
import android.util.Base64;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class SharedPreferencesUtil {
    private static final String PREFERENCE_NAME = "SharedPreference";
    private static final String ALGORITHM = "Blowfish";
    private static final String ENCRYPTION_MODE = "Blowfish/CBC/PKCS5Padding";
    private static final String IV = "abcdefgh";
    private static final String ENCRYPTION_PASS = "WatchGuardAssignment";

    public void setKey(Context context, String value) {
        if (value != null && !value.isEmpty())
            value = encryptKey(value);

        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
                .edit().putString("Key", value).apply();
    }

    public String getKey(Context context) {
        String key = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
                .getString("Key", null);

        if (key != null && !key.isEmpty())
            return decryptKey(key);

        return key;
    }

    public void setLabel(Context context, String value) {
        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
                .edit().putString("Label", value).apply();
    }

    public String getLabel(Context context) {
        return context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
                .getString("Label", null);
    }

    private String encryptKey(String value) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(ENCRYPTION_PASS.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(ENCRYPTION_MODE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(IV.getBytes()));

            byte[] values = cipher.doFinal(value.getBytes());

            return Base64.encodeToString(values, Base64.DEFAULT);
        } catch (BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException |
                NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        return "";
    }

    private String decryptKey(String value) {
        try {
            byte[] values = Base64.decode(value, Base64.DEFAULT);

            SecretKeySpec secretKeySpec = new SecretKeySpec(ENCRYPTION_PASS.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(ENCRYPTION_MODE);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(IV.getBytes()));

            return new String(cipher.doFinal(values));
        } catch (BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException |
                NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        return "";
    }
}