package com.example.wgproject.utils;

class TextUtils {
    private static final char[] HEX_ARRAY = "0123456789abcdef".toCharArray();

    static String separateHexBytes(String bytesInHex) {
        return insertPeriodically(bytesInHex, "|", 2);
    }

    private static String insertPeriodically(String text, String insert, int period) {
        StringBuilder builder = new StringBuilder(text.length() + insert.length() *
                (text.length() / period) + 1);

        int index = 0;
        String prefix = "";

        while (index < text.length()) {
            builder.append(prefix);
            prefix = insert;
            builder.append(text.substring(index,
                    Math.min(index + period, text.length())));
            index += period;
        }

        return builder.toString();
    }

    static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];

        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xff;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0f];
        }

        return new String(hexChars);
    }
}