package com.example.wgproject.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.wgproject.R;
import com.example.wgproject.events.BarcodeEvent;
import com.example.wgproject.events.TotpEvent;
import com.example.wgproject.models.KeyLabelModel;
import com.example.wgproject.utils.Constants;
import com.example.wgproject.utils.SharedPreferencesUtil;
import com.example.wgproject.utils.TotpUtils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity {
    private Button mGetKeyBtn;
    private Button mDeleteDataBtn;
    private TextView mLabelTextView;
    private TextView mTotpTextViewLeft;
    private TextView mTotpTextViewRight;

    private LinearLayout mProgressBarLayout;

    private SharedPreferencesUtil mSharedPreferencesUtil;

    private CountDownTimer mCountDownTimerLeft;
    private CountDownTimer mCountDownTimerRight;

    private int mCounterLeft;
    private int mCounterRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.main_activity);

            if (getSupportActionBar() != null)
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                        .getColor(R.color.colorPrimary)));

            mGetKeyBtn = findViewById(R.id.get_key_btn);
            mDeleteDataBtn = findViewById(R.id.delete_data_btn);
            mLabelTextView = findViewById(R.id.label_text_view);
            mTotpTextViewLeft = findViewById(R.id.totp_text_view_left);
            mTotpTextViewRight = findViewById(R.id.totp_text_view_right);
            mProgressBarLayout = findViewById(R.id.progress_bar_layout);

            EventBus.getDefault().register(this);

            mSharedPreferencesUtil = new SharedPreferencesUtil();
            String labelValue = mSharedPreferencesUtil.getLabel(this);

            if (labelValue != null && !labelValue.isEmpty())
                setKeyValues(labelValue);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onGetKeyClick(View v) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, 1);
        else {
            Intent myIntent = new Intent(this, ScannerActivity.class);
            startActivity(myIntent);
        }
    }

    public void onDeleteDataClick(View v) {
        TotpUtils totpUtils = TotpUtils.getInstance();
        totpUtils.cancelTimer();

        mSharedPreferencesUtil.setKey(this, null);
        mSharedPreferencesUtil.setLabel(this, null);

        mLabelTextView.setText("");
        mTotpTextViewLeft.setText("");
        mTotpTextViewRight.setText("");
        mGetKeyBtn.setVisibility(View.VISIBLE);
        mDeleteDataBtn.setVisibility(View.GONE);
        mProgressBarLayout.setVisibility(View.GONE);
    }

    private void setKeyValues(String labelValue) {
        mLabelTextView.setText(labelValue);
        mGetKeyBtn.setVisibility(View.GONE);
        mDeleteDataBtn.setVisibility(View.VISIBLE);
        mProgressBarLayout.setVisibility(View.VISIBLE);

        if (mCountDownTimerLeft != null)
            mCountDownTimerLeft.cancel();

        if (mCountDownTimerRight != null)
            mCountDownTimerRight.cancel();

        mCounterLeft = 0;
        mCounterRight = 0;

        ProgressBar progressBarLeft = findViewById(R.id.progress_bar_left);
        progressBarLeft.setProgress(mCounterRight);
        ProgressBar progressBarRight = findViewById(R.id.progress_bar_right);
        progressBarRight.setProgress(mCounterLeft);

        setCountDownTimerLeft(progressBarLeft);
        setCountDownTimerRight(progressBarRight);

        try {
            String key = mSharedPreferencesUtil.getKey(this);
            TotpUtils totpUtils = TotpUtils.getInstance();
            totpUtils.recursivelyGenerateOtp(key);
        } catch (Exception ex) {
            Log.d(Constants.LOG_TAG, "Error while generating TOTP");
            Toast.makeText(this, getString(R.string.error_totp),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void setCountDownTimerLeft(final ProgressBar progressBar) {
        mCountDownTimerLeft = new CountDownTimer(Constants.UPDATE_INTERVAL, Constants.TICK_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                progressBar.setProgress(mCounterRight * Constants.MAX_PROGRESS /
                        (Constants.UPDATE_INTERVAL / Constants.TICK_INTERVAL));
                mCounterRight++;
            }

            @Override
            public void onFinish() {
                mCounterRight = 0;
                progressBar.setProgress(0);
                mCountDownTimerLeft.start();
            }
        };

        mCountDownTimerLeft.start();
    }

    private void setCountDownTimerRight(final ProgressBar progressBar) {
        mCountDownTimerRight = new CountDownTimer(Constants.UPDATE_INTERVAL, Constants.TICK_INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                progressBar.setProgress(mCounterLeft * Constants.MAX_PROGRESS /
                        (Constants.UPDATE_INTERVAL / Constants.TICK_INTERVAL));
                mCounterLeft++;
            }

            @Override
            public void onFinish() {
                mCounterLeft = 0;
                progressBar.setProgress(0);
                mCountDownTimerRight.start();
            }
        };

        mCountDownTimerRight.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent myIntent = new Intent(this, ScannerActivity.class);
                startActivity(myIntent);
            } else
                Toast.makeText(this, getString(R.string.camera_permission),
                        Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBarcodeReadEvent(BarcodeEvent event) {
        try {
            Gson gson = new Gson();
            KeyLabelModel keyLabelModel = gson.fromJson(event.getBarcode(), KeyLabelModel.class);
            mSharedPreferencesUtil.setKey(this, keyLabelModel.getKey());
            mSharedPreferencesUtil.setLabel(this, keyLabelModel.getLabel());

            setKeyValues(keyLabelModel.getLabel());
        } catch (Exception ex) {
            Log.d(Constants.LOG_TAG, ex.getMessage());
            Toast.makeText(this, getString(R.string.wrong_qr_parse),
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTotpReceivedEvent(TotpEvent event) {
        mTotpTextViewLeft.setText(event.getTotp().substring(0, 3));
        mTotpTextViewRight.setText(event.getTotp().substring(3));
    }
}