package com.example.wgproject.activities;

import android.os.Bundle;
import android.view.ViewGroup;

import com.example.wgproject.R;
import com.example.wgproject.events.BarcodeEvent;
import com.google.zxing.Result;

import org.greenrobot.eventbus.EventBus;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerActivity extends BaseScannerActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mZXingScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.scanner_activity);
        initToolbar();

        ViewGroup contentFrame = findViewById(R.id.content_frame);
        mZXingScannerView = new ZXingScannerView(this);
        contentFrame.addView(mZXingScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mZXingScannerView.setResultHandler(this);
        mZXingScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mZXingScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        EventBus.getDefault().post(new BarcodeEvent(rawResult.getText()));
        finish();
    }
}