package com.example.wgproject.events;

public class BarcodeEvent {
    private String barcode;

    public BarcodeEvent(String barcode) {
        this.barcode = barcode;
    }

    public String getBarcode() {
        return barcode;
    }
}