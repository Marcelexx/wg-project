package com.example.wgproject.events;

public class TotpEvent {
    private String totp;

    public TotpEvent(String totp) {
        this.totp = totp;
    }

    public String getTotp() {
        return totp;
    }
}